import argparse
import copy
import functools
import logging
import sys
import typing
from pathlib import Path

from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QFrame, QVBoxLayout, QGroupBox, QWidget, QHBoxLayout, \
    QLineEdit, QPushButton, QStyle, QFileDialog, QTableView, QHeaderView

from exif_copier.copier import ExifCopier


logger = logging.getLogger("exif-copier")


class PathLineEdit(QFrame):

    def __init__(self, label: str, parent: QWidget = None):
        QFrame.__init__(self, parent)
        self.setLayout(QHBoxLayout())

        self._label = QLabel(label)
        self.layout().addWidget(self._label)
        self._line_edit = QLineEdit()
        self.layout().addWidget(self._line_edit)

        self._search_btn = QPushButton("browse")
        self._search_btn.setIcon(self.style().standardIcon(QStyle.SP_DirOpenIcon))

        def _search_clicked():
            selected_dir = QFileDialog.getExistingDirectory(self)
            self._line_edit.setText(selected_dir)

        self._search_btn.clicked.connect(_search_clicked)

        self.layout().addWidget(self._search_btn)

    @property
    def path(self) -> Path:
        return Path(self._line_edit.text())

    @path.setter
    def path(self, value: typing.Union[str, Path]):
        if isinstance(value, Path):
            value = value.as_posix()
        self._line_edit.setText(value)


class SettingsGroupBox(QGroupBox):

    def __init__(self, parent: QWidget = None):
        QGroupBox.__init__(self, "Settings", parent)

        self.setLayout(QVBoxLayout())

        self._source_le = PathLineEdit("source directory:")
        self.layout().addWidget(self._source_le)
        self._target_le = PathLineEdit("target directory:")
        self.layout().addWidget(self._target_le)

    @property
    def source_path(self) -> Path:
        return copy.copy(self._source_le.path)

    @source_path.setter
    def source_path(self, value: typing.Union[str, Path]):
        self._source_le.path = value

    @property
    def target_path(self) -> Path:
        return copy.copy(self._source_le.path)

    @target_path.setter
    def target_path(self, value: typing.Union[str, Path]):
        self._target_le.path = value


class ActionGroupBox(QGroupBox):

    def __init__(self, on_action: typing.Callable, actions: typing.List[str], parent: QWidget = None):
        QGroupBox.__init__(self, "Actions", parent)

        self._on_action = on_action
        self.setLayout(QHBoxLayout())

        self._btns = list()
        for a in actions:
            self._add_btn(a)

    def _add_btn(self, action: str):
        btn = QPushButton(action.replace("_", " "))
        btn.clicked.connect(
            functools.partial(
                self._on_action, action))
        self.layout().addWidget(btn)
        self._btns.append(btn)


class TableModel(QAbstractTableModel):

    COLUMNS = [
        "Source",
        "Target"
    ]

    def __init__(self, data):
        QAbstractTableModel.__init__(self)
        self._data = data

    def data(self, index: QModelIndex, role: int = ...) -> typing.Any:
        if role == Qt.DisplayRole:
            value = self._data[index.row()][index.column()]
            return str(value)

    def rowCount(self, parent: QModelIndex = QModelIndex()) -> int:
        return len(self._data)

    def columnCount(self, parent: QModelIndex = QModelIndex()) -> int:
        return len(self.COLUMNS)

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...) -> typing.Any:
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.COLUMNS[section]

            if orientation == Qt.Vertical:
                return str(section)


class PreviewGroupBox(QGroupBox):

    def __init__(self, parent: QWidget = None):
        QGroupBox.__init__(self, "Preview", parent)

        self._candidates = list()

        self.setLayout(QVBoxLayout())

        self._table = QTableView()
        header = self._table.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.Stretch)
        #self._table.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)
        self.layout().addWidget(self._table)

        self._table.setModel(TableModel(self._candidates))

        self.setLayout(QHBoxLayout())

    @property
    def candidates(self) -> typing.List[typing.Tuple[Path, Path]]:
        return copy.copy(self._candidates)

    @candidates.setter
    def candidates(self, value):
        self._candidates = value
        self._table.setModel(TableModel(self._candidates))


class MainWindow(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)

        self._exif_copier = ExifCopier(Path.cwd(), Path.cwd())

        self.setWindowTitle("Exif Copier")
        self.setMinimumSize(1000, 1000)

        self._frame = QFrame()
        self._layout = QVBoxLayout()
        self._frame.setLayout(self._layout)

        self._settings = SettingsGroupBox()
        self._layout.addWidget(self._settings)
        self._settings.source_path = Path.cwd()
        self._settings.target_path = Path.cwd()

        self._actions = ActionGroupBox(
            self._on_action,
            ["update_preview", "copy_exif_fields"])
        self._layout.addWidget(self._actions)

        self._preview = PreviewGroupBox()
        self._layout.addWidget(self._preview)

        self.setCentralWidget(self._frame)

    def _on_action(self, action_name: str):
        if action_name == "copy_exif_fields":
            self._exif_copier.source_path = self._settings.source_path
            self._exif_copier.target_path = self._settings.target_path
            self._exif_copier.copy_exif_fields(False)
        elif action_name == "update_preview":
            self._exif_copier.source_path = self._settings.source_path
            self._exif_copier.target_path = self._settings.target_path
            self._preview.candidates = self._exif_copier.copy_candidates
        else:
            logger.debug(f"received unknown action {action_name}")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug-logging", action="store_true",
                        default=False)
    args = parser.parse_args()

    logging.basicConfig(
        level=logging.WARN if not args.debug_logging else logging.DEBUG,
        format="%(asctime)s %(name)s [%(levelname).1s]: %(message)s",
        datefmt="%Y-%m-%d %H:%M")

    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()


if __name__ == "__main__":
    main()
