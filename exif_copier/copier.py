import copy
import logging
from pathlib import Path
from typing import Tuple, List

from PIL import Image
from PIL.ExifTags import TAGS

logger = logging.getLogger("exif-copier")
REVERSE_TAGS = {v: k for k, v in TAGS.items()}


class ExifCopier:

    supported_extensions = ["jpg", "jpeg", "jpe", "jfif", "png"]

    def __init__(self, source_path: Path, target_path: Path):
        self._source_path = source_path
        self._target_path = target_path

    @property
    def source_path(self) -> Path:
        return copy.copy(self._source_path)

    @source_path.setter
    def source_path(self, value):
        self._source_path = value

    @property
    def target_path(self) -> Path:
        return copy.copy(self._source_path)

    @target_path.setter
    def target_path(self, value):
        self._target_path = value

    @property
    def copy_candidates(self) -> List[Tuple[Path, Path]]:
        logger.debug(
            f"starting to search for candidates in source dir "
            f"\"{self._source_path}\" and target dir "
            f"\"{self._target_path}\"")

        candidates = list()

        source_files = list()
        for ext in self.supported_extensions:
            source_files += list(self._source_path.glob("*." + ext))

        target_files = list()
        for ext in self.supported_extensions:
            target_files += list(self._target_path.glob("*." + ext))

        for s_img in source_files:
            source_stem = s_img.stem
            for t_img in target_files:
                if t_img.suffixes != s_img.suffixes:
                    continue

                if self._source_path == self._target_path:
                    if t_img == s_img:
                        continue

                if t_img.stem.startswith(source_stem):
                    candidates.append((s_img, t_img))

        logger.debug(
            f"found the following candidates: {candidates}")

        targets = set([c[1] for c in candidates])
        candidates = list(filter(lambda c: c[0] not in targets, candidates))

        logger.debug(
            f"found removal of files which are a target and a source the "
            f"following candidates remain: {candidates}")

        return candidates

    def copy_exif_fields(self, complete_override: bool = True):
        candidates = self.copy_candidates

        for source, target in candidates:
            source_image = Image.open(source)
            source_exif = source_image.getexif()

            target_image = Image.open(target)
            target_exif = target_image.getexif()

            if complete_override:
                logger.debug(
                    f"doing a complete exif override for source {source} and "
                    f"target {target}")
                merged_exif = source_exif
            else:
                logger.debug(
                    f"merging exif for source {source} and target {target}")
                merged_exif = source_exif
                merged_exif.update(target_exif)

            hr_source_exif = {TAGS.get(k, k): v for k, v in source_exif.items()}
            logger.debug(
                f"source file {source} has the following exif tags: "
                f"{hr_source_exif}")
            hr_target_exif = {TAGS.get(k, k): v for k, v in target_exif.items()}
            logger.debug(
                f"target file {target} has the following exif tags: "
                f"{hr_target_exif}")
            hr_merged_exif = {TAGS.get(k, k): v for k, v in merged_exif.items()}
            logger.debug(
                f"merged exif tags to: {hr_merged_exif}")

            target_image.save(target, exif=merged_exif)
